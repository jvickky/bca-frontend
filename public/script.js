	// create the module and name it app
	var app = angular.module('app', ['ngRoute']);


	app.directive("prodinfo", function() {
	    return {
	        restrict: "E",
	        templateUrl: "/directives/prodinfo.html",
	        scope: {
	            "thumbnail": "@",
	            "name": "@",
	            "supText": "@",
	            "desc": "@",
	            "price": "@",
	            "discount": "@",
	            "category": "@",
	            "productType": "@",
	            "tags": "@",
	            "doDelete": "&doDelete",
	            "doUpdate": "&doUpdate",
	        }
	    };
	});

	// configure our routes
	app.config(function($routeProvider) {
	    $routeProvider

	        // route for the home page
	        .when('/', {
	            templateUrl: 'pages/home.html',
	            controller: 'mainController'
	        })

	        // route for the about page
	        .when('/banner', {
	            templateUrl: 'pages/banner.html',
	            controller: 'bannerController'
	        })

	        // route for the contact page
	        .when('/product', {
	            templateUrl: 'pages/product.html',
	            controller: 'productController'
	        });
	});

	// create the controller and inject Angular's $scope
	app.controller('mainController', function($scope) {
	    // create a message to display in our view
	    $scope.message = 'Everyone come and see how good I look!';
	});

	app.controller('bannerController', function($scope) {
	    $scope.message = 'Look! I am an about page.';
	});

	app.controller('productController', function($scope) {
	    $scope.message = 'Contact us! JK. This is just a demo.';
	    $scope.showAddView = false;
	    $scope.ButtonText = $scope.showAddView ? "Show List" : "Add Product"

	    $scope.addProd = () => {
	        $scope.showAddView = !$scope.showAddView;
	        $scope.ButtonText = $scope.showAddView ? "Show List" : "Add Product"
	    }
	});